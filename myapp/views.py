# text_normalization_app/views.py
from django.shortcuts import render
from .models import *
import re
import sklearn_crfsuite
from pythainlp.tokenize import word_tokenize
import joblib




def text_normalization(request):
    chunk = []
    normalized_result = ''  
    slot_filling_result = '' 
    word_token = []
    
    if request.method == 'POST':
        input_text = request.POST.get('input_text', '')  # 
        normalizer = ThaiTextNormalizertion()  
        chunk,word_token,slot_filling_result, normalized_result = normalizer.process_text(input_text)  # 
    word_token = ' | '.join(word_token)

    return render(request, 'myapp/text_normalization.html', {
        'chunk':chunk,
        'word_token':word_token,
        'normalized_result': ''.join(normalized_result),
        'slot_filling_result': slot_filling_result 
    })
