from django.db import models

import re
import sklearn_crfsuite
from pythainlp.tokenize import word_tokenize
import joblib

import json
import re
from collections import namedtuple
import glob
#

import pandas as pd


class ThaiTextNormalizertion:
    def __init__(self):
        self.crf_model_path = "myapp/crf_model/crf_model3.pkl"
        self.crf = sklearn_crfsuite.CRF()
        self.crf = joblib.load(self.crf_model_path)
        self.text_converter = TextConverter()

    def word23features(self,sentence, i):
        word = sentence[i]
        features = {
            'bias': 1.0,
            'word.lower()': word.lower(),
            'word.isupper()': word.isupper(),
            'word.istitle()': word.istitle(),
            'word.isdigit()': word.isdigit(),
            'word.isspace':word.isspace(),
            
        }

        if i > 1:
            word1 = sentence[i-2]
            features.update({
                '-2:word.lower()': word1.lower(),
                '-2:word.istitle()': word1.istitle(),
                '-2:word.isupper()': word1.isupper(),
                '-2:word.isdigit()': word1.isdigit(),
                '-2:word.isspace':word1.isspace(),
            })


        if i > 2:
            word2 = sentence[i-3]
            features.update({
                '-3:word.lower()': word2.lower(),
                '-3:word.istitle()': word2.istitle(),
                '-3:word.isupper()': word2.isupper(),
                '-3:word.isdigit()': word2.isdigit(),
                '-3:word.isspace':word2.isspace(),
            })


        if i < len(sentence)-2:
            word1 = sentence[i+2]
            features.update({
                '+2:word.lower()': word1.lower(),
                '+2:word.istitle()': word1.istitle(),
                '+2:word.isupper()': word1.isupper(),
                '+2:word.isdigit()': word1.isdigit(),
                '+2:word.isspace':word1.isspace(),
            })

        if i < len(sentence)-3:
            word2 = sentence[i+3]
            features.update({
                '+3:word.lower()': word2.lower(),
                '+3:word.istitle()': word2.istitle(),
                '+3:word.isupper()': word2.isupper(),
                '+3:word.isdigit()': word2.isdigit(),
                '+3:word.isspace':word2.isspace(),
            })
        return features

    def post_process_prediction(self,tokens, labels):
        normalized_tokens = []
        thai_text = []
        for token, label in zip(tokens, labels):
            if label in ['I-social','B-social','O']:
                thai_text.append(token)
        for token, label in zip(tokens, labels):
            if label in ['I-phone', 'I-password','I-student_id','B-phone', 'B-password','B-student_id']:
                normalized_tokens.append(self.text_converter.convert_number_to_text(token))
            elif label in ['B-temperature','I-temperature','B-dash','I-dash','I-size', 'I-price', 'I-quantity','I-distance','I-speed','I-day','I-time','I-land_size','I-asset_value','I-salary','I-number','B-size', 'B-price', 'B-quantity','B-distance','B-speed','B-day','B-time','B-land_size','B-asset_value','B-salary'] and re.match(r'\d+', token):
                normalized_tokens.append(self.text_converter.normalize_number(token))
            elif label in ['I-address','I-lottery_online','I-bus_num','I-iso_num','I-marine_num','I-lottery','I-id_card','I-registration_num','I-license_plate','I-highway','B-address','B-lottery_online','B-bus_num','B-iso_num','B-marine_num','B-lottery','B-id_card','B-registration_num','B-license_plate','B-highway'] :
                normalized_tokens.append(self.text_converter.convert_number_to_text(token))
            elif  label in ['B-dash','I-dash'] and re.match(r'-', token):
                normalized_tokens.append(self.text_converter.convert_mark(token))
            elif  label in ['B-temperature','I-temperature'] and re.match(r'-', token):
                normalized_tokens.append('ลบ')
            elif label in ['I-speed','B-speed'] and re.match(r'/', token):
                normalized_tokens.append('ต่อ')
            elif label in ['I-day','B-day'] and re.match(r'-', token):
                normalized_tokens.append('ถึง')
            elif label in ['I-url','B-url']:
                normalized_tokens.append(self.text_converter.conver_url(token))
            elif label in ['I-abb','B-abb']:
                normalized_tokens.extend(self.text_converter.convert_abb(token))
            elif label in ['I-engSupper','B-engSupper']:
                normalized_tokens.append(self.text_converter.conver_engSupper(token))
            elif label in ['I-engLower','B-engLower']:
                normalized_tokens.append(self.text_converter.convert_engLower(token))
            elif label in ['I-social','B-social','O'] and token != 'วันนี้':
                # normalized_tokens.append(repeated_words(''.join(token)))
                normalized_tokens.append(self.text_converter.repeated_words(token))
            elif label in ['B-mark','I-mark']:
                normalized_tokens.append(self.text_converter.convert_mark(token))
            elif label in ['B-number']:
                normalized_tokens.append(self.text_converter.normalize_number(token))
            else:
                normalized_tokens.append(token)
        return normalized_tokens

    def should_tag_as_O(self, token):
        return not token.isdigit()

    def predict(self, sentence):
        text = Lexer()
        output = text.lexer(sentence)
        tmp = []  
        chunk = []
        for item in output:
            
            tmp.append(item.value)
            chunk.append(item.type)
            
        tokens = word_tokenize(''.join(tmp), engine="newmm")
        features = [self.word23features(tokens, i) for i in range(len(tokens))]
        labels = self.crf.predict_single(features)

        merged_tokens = []
        merged_labels = []
        temp_token = ""
        engchar = []
        previous_label = None
        for token, label in zip(tokens, labels):
                    engchar.append(token)

                    if self.should_tag_as_O(token) and re.fullmatch(r'$|&|@|฿|://|.|°', token)  and token != ' '  and token != '–'  and token != '-' and token != '/':
                        if temp_token:
                            merged_tokens.append(temp_token)
                            temp_token = ""
                        is_previous_number = False
                        if previous_label != 'I-mark':
                            merged_tokens.append(token)
                            merged_labels.append('B-mark')
                        else:
                            merged_tokens.append(token)
                            merged_labels.append('I-mark')
                        previous_label = 'I-mark'
                    elif not self.should_tag_as_O(token)  and label == 'I-address':
                        if temp_token:
                            merged_tokens.append(temp_token)
                            temp_token = ""
                        is_previous_number = False
                        if previous_label != 'I-address':
                            merged_tokens.append(token)
                            merged_labels.append('B-address')
                        else:
                            merged_tokens.append(token)
                            merged_labels.append('I-address')
                        previous_label = 'I-address'

                    elif not self.should_tag_as_O(token)  and label == 'I-lottery':
                        if temp_token:
                            merged_tokens.append(temp_token)
                            temp_token = ""
                        is_previous_number = False
                        if previous_label != 'I-lottery':
                            merged_tokens.append(token)
                            merged_labels.append('B-lottery')
                        else:
                            merged_tokens.append(token)
                            merged_labels.append('I-lottery')
                        previous_label = 'I-lottery'

                    elif self.should_tag_as_O(token) and re.fullmatch(r'[ก-ฮ]{1,4}\.([ก-ฮ]{1,4}\.)*', ''.join(token)) :
                        if temp_token:
                            merged_tokens.append(temp_token)
                            temp_token = ""
                        if previous_label != 'I-abb':
                            merged_tokens.append(token)
                            merged_labels.append('B-abb')
                        else:
                            merged_tokens.append(token)
                            merged_labels.append('I-abb')
                        previous_label = 'I-abb'
                    elif self.should_tag_as_O(token) and re.fullmatch(r'[a-z_][a-z_]*[a-z_]', token) :
                        if temp_token:
                            merged_tokens.append(temp_token)
                            temp_token = ""
                        is_previous_number = False
                        merged_tokens.append(token)
                        merged_labels.append('B-engLower')
                    elif self.should_tag_as_O(token) and re.fullmatch(r'[A-Z_][A-Z_]*[A-Z_]', token) :
                        if temp_token:
                            merged_tokens.append(temp_token)
                            temp_token = ""
                        is_previous_number = False
                        merged_tokens.append(token)
                        merged_labels.append('B-engSupper')



                    elif self.should_tag_as_O(token) and re.fullmatch(r'[ก-ฮ]{1,4}\.([ก-ฮ]{1,4}\.)*', ''.join(token)) :
                        
                        if temp_token:
                            merged_tokens.append(temp_token)
                            temp_token = ""
                        is_previous_number = False
                        merged_tokens.append(token)
                        merged_labels.append('B-abb')

                    elif re.fullmatch(r'[0-9]{1,3}\,[0-9]{3}(,[0-9]{3})*(\.[0-9]*)*',token):
                        is_previous_number = False
                        merged_tokens.append(token)
                        merged_labels.append('B-price')

                    elif self.should_tag_as_O(token)  and label != 'I-abb'  and label != 'I-social' and label != 'I-eng' and token != '-'and token != 'x' and token != '/' :
                        if temp_token:
                            merged_tokens.append(temp_token)
                            temp_token = ""
                        is_previous_number = False
                        merged_tokens.append(token)
                        merged_labels.append('O')


                    elif re.fullmatch(r'[ก-ฮ]{1,4}\.([ก-ฮ]{1,4}\.)*', ''.join(token)) :
                        is_previous_number = False
                        merged_tokens.append(token)
                        merged_labels.append('B-abb')

                    else:
                        if not token.startswith('0') and token.isdigit() and label == 'O' :
                            is_previous_number = False
                            merged_tokens.append(token)
                            merged_labels.append('B-number')
                        elif token.startswith('0') and token.isdigit() and label == 'O':
                            is_previous_number = False
                            merged_tokens.append(token)
                            merged_labels.append('I-phone')
                        else:
                            if temp_token:
                                merged_tokens.append(temp_token)
                                temp_token = ""
                            is_previous_number = False
                            merged_tokens.append(token)
                            merged_labels.append(label)

        normalized_tokens = self.post_process_prediction(merged_tokens, merged_labels)
        normalized_sentence = (normalized_tokens)

        # print(chunk)
        # print(output)
        return list(zip(chunk,tmp)),tokens,list(zip(merged_tokens, merged_labels)), normalized_sentence

    def process_text(self, text):
        tmp,word_token,result, normalized_result = self.predict(text)
        return tmp,word_token,result,normalized_result



class TextConverter:
    def __init__(self):
        self.phone_df = pd.read_csv("myapp/data/phone.csv")
        self.mark_df = pd.read_csv("myapp/data/เครื่องหมาย.csv")
        self.url_df = pd.read_csv("myapp/data/url.csv")
        self.abb_df = pd.read_csv("myapp/data/ตัวย่อ.csv")
        self.eng_df = pd.read_csv("myapp/data/คำอ่านภาษาอังกฤษแบบปกติ.csv")

        self.phone = dict(zip(self.phone_df["key"], self.phone_df["value"]))
        self.mark = dict(zip(self.mark_df["key"], self.mark_df["value"]))
        self.url = dict(zip(self.url_df["key"], self.url_df["value"]))
        self.abb = dict(zip(self.abb_df["key"], self.abb_df["value"]))
        self.eng = dict(zip(self.eng_df["key"], self.eng_df["value"]))

    def normalize_number(self, token):
        return self.thai_num2text(token)

    def repeated_words(self, text):
        self.NORMALIZE_RULE1 = [
            "ะ", "ั", "็", "า", "ิ", "ี", "ึ", "่", "ํ", "ุ",
            "ู", "ใ", "ไ", "โ", "ื", "่", "้", "๋", "๊", "ึ",
            "์", "๋", "ำ", "ก", "ข", "ฃ", "ค", "ฅ", "ฆ", "ง",
            "จ", "ฉ", "ช", "ซ", "ฌ", "ญ", "ฎ", "ฏ", "ฐ", "ฑ",
            "ฒ", "ณ", "ด", "ต", "ถ", "ท", "ธ", "น", "บ", "ป",
            "ผ", "ฝ", "พ", "ฟ", "ภ", "ม", "ย", "ร", "ล", "ว",
            "ศ", "ษ", "ส", "ห", "ฬ", "อ", "ฮ"
        ]

        self.NORMALIZE_RULE2 = [
            ("เเ", "แ"),
            ("ํ(t)า", "\\1ำ"),
            ("ํา(t)", "\\1ำ"),
            ("([่-๋])([ัิ-ื])", "\\2\\1"),
            ("([่-๋])([ูุ])", "\\2\\1"),
            ("ำ([่-๋])", "\\1ำ"),
            ("(์)([ัิ-ู])", "\\2\\1"),
            ("น{3,}", ""),
            ("วันนี้น", "วันนี้"),
            ("มากก", "มาก"),
            ("ม{3,}", ""),
            ("บ{2,}", ""),
            ("ก{2,}", ""),
            ("ด{2,}", ""),
            ("ง{2,}", ""),
            # ("[ก-ฮ]{3}", ""),
             ("า{2,}", ""),

        ]
        for data in self.NORMALIZE_RULE2:
            text = re.sub(data[0].replace("t", "[่้๊๋]"), data[1], text)
        for data in list(zip(self.NORMALIZE_RULE1, self.NORMALIZE_RULE1)):
            text = re.sub(data[0].replace("t", "[่้๊๋]") + "+", data[1], text)
        return text

    def convert_number_to_text(self,text):
        converted_text = []
        keys_rule = self.phone.keys()
        for i in range(len(text)):
            word = text[i]
            if word in keys_rule :
                converted_text.append(self.phone[word])
        return ''.join(converted_text)


    def conver_url(self,text):
        converted_url = []
        if text in self.url:
            converted_url.append(self.url.get(text, ""))
        else:
            for char in text:
                thai_char = self.url.get(char.upper(), char)
                converted_url.append(thai_char)
        return ' '.join(converted_url)

    def conver_engSupper(self,text):
        converted_url = []
        if text in self.url:
            converted_url.append(self.eng.get(text, ""))
        else:
            for char in text:
                thai_char = self.url.get(char.upper(), char)
                converted_url.append(thai_char)
        return ''.join(converted_url)

    def convert_engLower(self, text):
        if text == text:
            return self.eng.get(text, text)
        return ' '.join(text)

    def convert_abb(self, text):
        if text == text:
            return self.abb.get(text, text)
        return ' '.join(text)
    def convert_mark(self,text):
        if text == text:
            return self.mark.get(text, text)
        return ' '.join(text)

    def thai_num2text(self, number):
        thai_number = {
            0: "ศูนย์",
            1: "หนึ่ง",
            2: "สอง",
            3: "สาม",
            4: "สี่",
            5: "ห้า",
            6: "หก",
            7: "เจ็ด",
            8: "แปด",
            9: "เก้า",
            "-": "ลบ",
            "/": "หาร",
            "+": "บวก",
            "*": "คูณ",
            "x": "คูณ",
        }

        is_negative = False
        if number.startswith("-"):
            is_negative = True
            number = number[1:]

        number = number.replace(",", "")

        if "." in number:
            integer_part, decimal_part = number.split(".")
        else:
            integer_part = number
            decimal_part = ""

        unit = {
            0: "ศูนย์",
            1: "สิบ",
            2: "ร้อย",
            3: "พัน",
            4: "หมื่น",
            5: "แสน",
            6: "ล้าน"
        }

        def unit_process(val):
            length = len(val) > 1
            result = ''

            for index, current in enumerate(map(int, val)):
                if current:
                    if index:
                        result = unit.get(index, '') + result

                    if length and current == 1 and index == 0:
                        result += 'เอ็ด'
                    elif index == 1 and current == 2:
                        result = 'ยี่' + result
                    elif index != 1 or current != 1:
                        result = thai_number.get(current, '') + result

            return result

        result = unit_process(integer_part[::-1])

        if decimal_part:
            result += 'จุด'
            for digit in decimal_part:
                result += thai_number.get(int(digit), '')

        if is_negative:
            result = thai_number.get("-") + result

        return result



class Lexer(object):
    def __init__(self, word_sep="|",abb_dict_path=None,encrypt_pw=None):
        self.REXS = [ r'(?P<PHONEME>\{[a-z @0-4\^]+\})',
            r'(?P<ANIM_TAG>\<[A-Za-z0-9_\-]+\>)',
           r'(?P<ENG_CHAR>[a-zA-Z_][a-zA-Z_]*[a-zA-Z_])',
           r'(?P<ENG_CHAR_BLOCK>\[[a-zA-Z_][a-zA-Z_0-9 ]*\])',
           r'(?P<THAI_MAI_YAMOK>[ๆ]+)',
           r'(?P<THAI_PAI_YAN_YAI>ฯลฯ)',
           r'(?P<THAI_NUM_TEXT>((หนึ่ง|สอง|สาม|สี่|ห้า|หก|เจ็ด|แปด|เก้า)(ล้าน|หมื่น|แสน|พัน|ร้อย))|((หนึ่ง|สอง|สาม|สี่|ห้า|หก|เจ็ด|แปด|เก้า|ยี่)(สิบ)(หนึ่ง|สอง|สาม|สี่|ห้า|หก|เจ็ด|แปด|เก้า|เอ็ด)?))',
           r'(?P<THAI_ABB>[ก-ฮ]{1,4}\.([ก-ฮ]{1,4}\.)*)',
           r'(?P<THAI_CHAR_ABB>[ก-ฮะ-ๅ็-ํ]+\.([ก-ฮ]{1,4}\.)*)',
           r'(?P<THAI_CHAR>[ก-ฮะ-ๅ็-ํ]+)',
           r'(?P<NUMBER_COMMA>[0-9]{1,3}\,[0-9]{3}(,[0-9]{3})*(\.[0-9]*)*)',
           r'(?P<NUMBER>\d+(?:\.\d+)?)',
           r'(?P<MATH>[=%\+\-\*/])',
           r'(?P<WHITESPACE>\s+)',
          #  r'(?P<EOF>\Z)',
           r'(?P<OTHER>.)'] # catch everything else, which is an error

        self.TOKEN_TYPE2ID = ["PHONEME",
                              "ANIM_TAG",
                              "ENG_CHAR",
                              "ENG_CHAR_BLOCK",
                              "THAI_MAI_YAMOK",
                              "THAI_PAI_YAN_YAI",
                              "THAI_NUM_TEXT",
                              "THAI_ABB",
                              "THAI_CHAR_ABB",
                              "THAI_CHAR",
                              "NUMBER_COMMA",
                              "NUMBER",
                              "MATH",
                              "WHITESPACE",
                              # "EOF",
                              "OTHER"
        ]




    def lexer(self,text):
        Token = namedtuple('Token', ['type','value'])
        tokenizer = re.compile('|'.join(self.REXS))
        seen_error = False
        for m in tokenizer.finditer(text):
            #if m.lastgroup != 'WHITESPACE': #ignore whitespace
            #if m.lastgroup == 'ERROR':
            #    if not seen_error:
            #        yield Token(m.lastgroup, m.group())
            #        seen_error = True # scan until we find a non-error input
            #else:
            yield Token(m.lastgroup, m.group())
            #    seen_error = False
            #else:
            #    seen_error = False
        return Token

    def __call__(self,text,ws_bi,json_output=False,verbose=False):
        token_bi = []
        token_type = []
        token_len = []
        ws_text = text
        #ws_bi =tags["OUTPUT__0"][0].tolist()
        #print(ws_bi)
        cnt = 0
        token_data = []
        word_list = []
        for token in self.lexer(text):
            if verbose:
                print(token)

            pre_process_phoneme = ""

            tmp_text = ws_text[cnt:cnt+len(token.value)]
            nor_text = ""
            if token.type=="THAI_ABB": #ตัวย่อ
                nor_text,pre_process_phoneme=self.symphon.abb2phoneme(token.value)
            elif token.type in ["NUMBER","NUMBER_COMMA","MATH"]: #ตัวเลข

                pre_process_phoneme=self.symphon.number2phoneme(token.value)

            if token.type=="THAI_CHAR" or token.type=="THAI_CHAR_ABB": #แบ่งคำ

                _w = self.id2text([ws_bi[cnt:cnt+len(token.value)]],[ws_text[cnt:cnt+len(token.value)]],sep="|")


                for x in _w[0].split("|"):
                    if "." in x: #convert type to ABB
                        nor_text,pre_process_phoneme=self.symphon.abb2phoneme(x)
                        if token_data[-1][0]=="THAI_ABB":
                            token_data[-1][1] += nor_text
                            token_data[-1][-1] = "" #clear phoneme
                        else:
                            token_data.append(["THAI_ABB",nor_text,x,pre_process_phoneme.strip()])
                    else:
                        token_data.append(["THAI_CHAR",x,x,pre_process_phoneme.strip()])


            else:
                new_text= ws_text[cnt:cnt+len(token.value)] if nor_text=="" else nor_text
                token_data.append([token.type,new_text,token.value,pre_process_phoneme.strip()])

            cnt+=len(token.value)
        output_data = []
        for item in token_data:
            output_data.append({"src_text":item[2],"nor_text":item[1],"token_type":item[0],"phoneme":item[-1]})

        if json_output:
            return json.dumps(output_data)
        else:
            return output_data
    def id2text(self,output_token,input_text=None,sep='|',sep_id=2, pad_idx=0):
        outputs = []
        for i in range(len(output_token)):
            _output = ""
            for j in range(len(output_token[i])):
                if output_token[i][j]==sep_id:

                    if output_token[i][j] ==pad_idx:
                        break
                    if j > 0:
                        _output+=sep

                if input_text is not None:
                    try:
                        _output+=input_text[i][j]
                    except Exception as e:
                        pass
            outputs.append(_output)
        return outputs
