from django.urls import path
from .views import text_normalization

urlpatterns = [
    path('', text_normalization, name='text-normalization'),
]
